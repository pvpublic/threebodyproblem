#!/usr/bin/python3

import math

class Vec2D:
    def __init__(self):
        self.theta = 0
        self.r = 0

    def setMagnitude(self, mag):
        self.r = mag

    def setDirection(self, angle):
        self.theta = angle

    def getMagnitude(self):
        return self.r

    def getDirection(self):
        return self.theta

    def resolved(self):
        return (math.cos(self.theta) * self.r, math.sin(self.theta) * self.r)

    def sum(v1, v2):
        s = Vec2D()
        base = (v1.getMagnitude() * math.cos(v1.getDirection())) + (v2.getMagnitude() * math.cos(v2.getDirection()))
        height = (v1.getMagnitude() * math.sin(v1.getDirection())) + (v2.getMagnitude() * math.sin(v2.getDirection()))
        s.setMagnitude(math.sqrt(math.pow(base, 2) + math.pow(height, 2)))
        if base != 0:
            s.setDirection(math.atan(height / base))
            if base / abs(base) == -1:
               s.setDirection(s.getDirection() + math.pi)
        elif height != 0:
            s.setDirection(-math.pi * height/abs(height))
        else:
            s.setDirection(0)
        return s

    def scale(vector, scalar):
        vector.setMagnitude(vector.getMagnitude() * scalar)
        return vector

if __name__ == "__main__":
    v = Vec2D()
    w = Vec2D()
    v.setDirection(math.radians(45))
    v.setMagnitude(1)
    w.setDirection(math.radians(135))
    w.setMagnitude(1)
    s = Vec2D.sum(v, w)
    print(s.getMagnitude())
    print(math.degrees(s.getDirection()))

    v.setDirection(math.radians(-60))
    v.setMagnitude(1)
    w.setDirection(math.radians(45))
    w.setMagnitude(1)
    s = Vec2D.sum(v, w)
    print(s.getMagnitude())
    print(math.degrees(s.getDirection()))
