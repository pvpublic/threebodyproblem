#!/usr/bin/python3

import math
import pygame
import time
from random import randint
from pygame.locals import * 
from vec import Vec2D

WIDTH = 1280
HEIGHT = 720

pygame.init()
pygame.display.set_caption("Three Body Problem Model")

screen = pygame.display.set_mode([WIDTH, HEIGHT])
background = pygame.image.load("assets/bg.png")

G = 10
UpdateInterval = 1

class Ball:
    def __init__(self, initialX, initialY, mass=1):
        self.velocity = Vec2D()
        self.position = (initialX, initialY)
        self.image = pygame.image.load("assets/Ball.png")
        self.mass = mass

    def distanceTo(self, anotherBall):
        return math.sqrt(math.pow(anotherBall.getPosition()[0] - self.getPosition()[0], 2) + math.pow(anotherBall.getPosition()[1] - self.getPosition()[1], 2))

    def directionTo(self, anotherBall):
        xDiff = anotherBall.getPosition()[0] - self.getPosition()[0]
        yDiff = anotherBall.getPosition()[1] - self.getPosition()[1]
        if xDiff != 0 and yDiff != 0:
            d = math.atan(yDiff / xDiff)
            if xDiff / abs(xDiff) == -1:
                d += math.pi
        elif yDiff != 0:
            d = yDiff / abs(yDiff) * -math.pi/2
        elif xDiff != 0:
            d = xDiff / abs(xDiff) * -math.pi
        else:
            d = 0
        return d

    def getPosition(self):
        return self.position

    def getImage(self):
        return self.image

    def getVelocity(self):
        return self.velocity

    def getMass(self):
        return self.mass

    def setVelocity(self, v):
        self.velocity = v

    def updatePosition(self, time):
        posVector = Vec2D.scale(self.velocity, time)
        self.position = (self.position[0] + posVector.resolved()[0], self.position[1] + posVector.resolved()[1])

ballCnt = 20
balls = []
for i in range(ballCnt):
    balls.append(Ball(randint(-WIDTH//2, WIDTH//2), randint(-HEIGHT//2, HEIGHT//2), 1))

running = True

while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

    for i in range(ballCnt):
        acceleration = Vec2D()
        for b in balls[:i] + balls[i+1:]:
            f = Vec2D()
            f.setMagnitude(G * b.getMass() / math.pow(balls[i].distanceTo(b), 2)) # Not multiplying mass of body in question
            f.setDirection(balls[i].directionTo(b))
            acceleration = Vec2D.sum(acceleration, f) # Not dividing force by mass since we didn't multiply it in the first place
        balls[i].setVelocity(Vec2D.sum(balls[i].getVelocity(), Vec2D.scale(acceleration, UpdateInterval)))
        balls[i].updatePosition(UpdateInterval)
    
    screen.blit(background, (0, 0))
    for i in range(ballCnt):
        screen.blit(balls[i].getImage(), (balls[i].getPosition()[0] + WIDTH/2, balls[i].getPosition()[1] + HEIGHT/2))
    pygame.display.update()
    time.sleep(0.01)
