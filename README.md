# Three Body Problem Model #

A model to visualize astrophysics' [three-body problem](https://en.wikipedia.org/wiki/Three-body_problem), written in Python and Pygame

### Prerequisites ###

* Python 3
* Pygame

### Getting Started ###

* Clone this repository with `git clone https://PrithviVishak@bitbucket.org/pvpublic/threebodyproblem.git`, or just download it as a ZIP.
* Navigate to the repository's folder and run `main.py`
```
cd ~/Documents/threebodyproblem
./main.py
```
